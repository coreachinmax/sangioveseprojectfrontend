import React from 'react'
import ReactDOM from 'react-dom/client'
import {createBrowserRouter, RouterProvider,} from 'react-router-dom'
import './index.css'
import Root from './pages/root'
import ErrorPage from './pages/error-page'
import reportWebVitals from './reportWebVitals'
import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'
import CreateWinePage from './pages/create-wine-page'
import HomePage from './pages/home-page'
import CreateRegionPage from './pages/create-region-page'
import CreateWineTypePage from './pages/create-wine-type-page'
import WineTablePage from './pages/wine-table-page'
import RegionTablePage from './pages/region-table-page'
import WineTypeTablePage from './pages/wine-type-table-page'
import {SnackbarProvider} from "notistack";

const router = createBrowserRouter([
    {
        path: '/',
        element: <Root/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                // path: "product",
                index: true,
                element: <HomePage/>,
            },
            {
                path: 'create-wine',
                element: <CreateWinePage/>,
            },
            {
                path: 'update-wine/:id',
                element: <CreateWinePage/>,
            },
            {
                path: 'create-region',
                element: <CreateRegionPage/>,
            },
            {
                path: 'update-region/:id',
                element: <CreateRegionPage/>,
            },
            {
                path: 'create-wine-type',
                element: <CreateWineTypePage/>,
            },
            {
                path: 'update-wine-type/:id',
                element: <CreateWineTypePage/>,
            },
            {
                path: 'wine-table',
                element: <WineTablePage/>,
            },
            {
                path: 'region-table',
                element: <RegionTablePage/>,
            },
            {
                path: 'wine-type-table',
                element: <WineTypeTablePage/>,
            },
        ],
    }
])

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <SnackbarProvider maxSnack={3} autoHideDuration={5000}>
            <RouterProvider router={router}/>
        </SnackbarProvider>
    </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
