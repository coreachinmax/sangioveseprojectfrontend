import { Outlet } from 'react-router-dom'
import MainHeader from '../components/header/main-header'

export default function Root () {

  return (
    <>
      <MainHeader/>

      <div>
        <Outlet/>
      </div>
    </>
  )
}
