import WineList from '../components/wine-list/wine-list'
import Container from '@mui/material/Container'

export default function HomePage () {
  return (
    <>
      <Container maxWidth="xl">
        <WineList/>
      </Container>
    </>
  )
}
