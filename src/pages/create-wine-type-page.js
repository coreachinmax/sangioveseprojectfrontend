import WineTypeForm from "../components/forms/wine-type-form";

export default function CreateWineTypePage () {
    return (
        <>
            <WineTypeForm/>
        </>
    )
}
