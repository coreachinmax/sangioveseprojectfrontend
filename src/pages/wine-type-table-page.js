import WineTypeTable from "../components/tables/wine-type-table";

export default function WineTypeTablePage () {
    return (
        <>
            <WineTypeTable/>
        </>
    )
}
