import { axiosInstance } from '../axios/axios'

const endpoint = `${axiosInstance.defaults.baseURL}/winetypes/`

export function getWineType(id){
  return axiosInstance.get(`${endpoint}${id}`).then(res => res.data)
}
export function getWineTypes(){
  return axiosInstance.get(endpoint).then(res => res.data)
}

export function createWineType(data){
  return axiosInstance.post(endpoint, data)
}

export function updateWineType(id, data){
  return axiosInstance.put(`${endpoint}${id}`, data).then(res => res.data)
}

export function deleteWineType(id){
  return axiosInstance.delete(`${endpoint}${id}`)
}
