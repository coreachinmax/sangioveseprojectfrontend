import { axiosInstance } from '../axios/axios'

const endpoint = `${axiosInstance.defaults.baseURL}/wines/`

export function getWine(id){
  return axiosInstance.get(`${endpoint}${id}`).then(res => res.data)
}

export function getWines(){
  return axiosInstance.get(endpoint).then(res => res.data)
}

export function getWinesByRegionAndType(region, type){
  return axiosInstance.get(`${endpoint}?regionId=${region}&wineTypeId=${type}`).then(res => res.data)
}

export function createWine(data){
  return axiosInstance.post(endpoint, data).then(res => res.data)
}

export function updateWine(id, data){
  return axiosInstance.put(`${endpoint}${id}`, data).then(res => res.data)
}

export function deleteWine(id){
  return axiosInstance.delete(`${endpoint}${id}`)
}
