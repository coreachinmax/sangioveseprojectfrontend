import { axiosInstance } from '../axios/axios'

const endpoint = `${axiosInstance.defaults.baseURL}/regions/`

export function getRegion(id){
  return axiosInstance.get(`${endpoint}${id}`).then(res => res.data)
}
export function getRegions(){
  return axiosInstance.get(endpoint).then(res => res.data)
}

export function createRegion(data){
  return axiosInstance.post(endpoint, data)
}

export function updateRegion(id, data){
  return axiosInstance.put(`${endpoint}${id}`, data).then(res => res.data)

}

export function deleteRegion(id){
  return axiosInstance.delete(`${endpoint}${id}`)
}
