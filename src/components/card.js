import * as React from 'react'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import ShareIcon from '@mui/icons-material/Share'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart'
import Box from '@mui/material/Box'
import { centsToDollars } from '../helpers/price'
import { Chip, Stack } from '@mui/material'
import { truncate } from '../helpers/text'

export default function ProductCard(props) {
    return (
        <Card sx={{maxWidth: 345}}>
            <CardHeader
                titleTypographyProps={{
                    fontSize: 19,
                }}
                title={props.itemName}
                sx={{height: '50px'}}
            />
            <CardMedia
                component="img"
                height="194"
                image={props.imageLink}
                sx={{objectFit: "contain"}}
            />
            <Box>
                <Stack direction="row" spacing={1} sx={{mt: 2, ml: 2}}>
                    <Chip label={props.wineType.wineTypeName} variant="outlined"/>
                    <Chip label={props.region.regionName} variant="outlined"/>
                </Stack>
            </Box>
            <CardContent sx={{height: '70px'}}>
                <Typography variant="body2" color="text.secondary">
                    {truncate(props.description, 150)}
                </Typography>
            </CardContent>
            <CardActions sx={{display: 'flex', justifyContent: 'space-between'}} disableSpacing>
                <Typography variant="subtitle1" color="text.secondary">
                    {centsToDollars(props.price)}$
                </Typography>
                <Box>
                    <IconButton aria-label="add to cart">
                        <AddShoppingCartIcon/>
                    </IconButton>
                    <IconButton aria-label="share">
                        <ShareIcon/>
                    </IconButton>
                </Box>
            </CardActions>
        </Card>
    )
}
