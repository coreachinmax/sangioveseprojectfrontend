import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import RegionSelect from '../forms/region-select'
import TypeSelect from '../forms/type-select'

export default function SearchFilters ({onChange}) {
  const [open, setOpen] = React.useState(false)
  const [region, setRegion] = React.useState(null)
  const [type, setType] = React.useState(null)

  const handleTypeChange = (event) => {
    setType(event?.target.value)
  }

  const handleRegionChange = (event) => {
    setRegion(event?.target.value)
  }

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleOk = () => {
    onChange({region, type})
    handleClose()
  }

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false)
    }
  }

  return (
    <div>
      <Button onClick={handleClickOpen}>Search filters</Button>
      <Dialog disableEscapeKeyDown open={open} onClose={handleClose}>
        <DialogTitle>Select type and region</DialogTitle>
        <DialogContent>
          <Box component="form" sx={{display: 'flex', flexWrap: 'wrap'}}>
            <TypeSelect onChange={handleTypeChange}></TypeSelect>
            <RegionSelect onChange={handleRegionChange}></RegionSelect>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleOk}>Ok</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
