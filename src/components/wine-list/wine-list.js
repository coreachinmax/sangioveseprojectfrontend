import * as React from 'react'
import { useEffect } from 'react'
import ProductCard from '../card'
import SearchFilters from '../header/search-filters'
import Grid from '@mui/material/Unstable_Grid2'
import { getWines, getWinesByRegionAndType } from '../../services/WineService'
import to from 'await-to-js'
import Button from '@mui/material/Button'

export default function WineList () {
  const [wines, setWines] = React.useState([])
  const [searchFilters, setSearchFilters] = React.useState({
    region: null,
    type: null
  })

  function clearFilters () {
    setSearchFilters({
      region: null,
      type: null
    })
    fetchData ()
  }

  async function fetchData () {
    const [err, data] = await to(getWines())

    if (err) {
      console.error(err)
      return
    }
    setWines(data)
  }

  async function fetchDataWithFilters () {
    const [err, data] = await to(getWinesByRegionAndType(searchFilters.region, searchFilters.type))
    if (err) {
      console.error(err)
      return
    }
    setWines(data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  useEffect(() => {
    if (searchFilters.region && searchFilters.type) {
      fetchDataWithFilters()
    }
  }, [searchFilters.region, searchFilters.type])

  return (
    <>
      <h1>Sangiovese</h1>
      <div style={{'marginBottom': '20px'}}>
        { (searchFilters.region && searchFilters.type)
          ? <Button variant="outlined" color="secondary" onClick={clearFilters}>Clear filters</Button>
          : <SearchFilters onChange={setSearchFilters}/> }
      </div>

      <Grid container spacing={2}>
        {wines?.map((wine) => (
          <Grid xs={12} sm={6} md={4} lg={3} key={wine.id}>
            <ProductCard {...wine}/>
          </Grid>
        ))}
      </Grid>
    </>
  )

}
