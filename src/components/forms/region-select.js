import * as React from 'react'
import { useEffect } from 'react'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import { getRegions } from '../../services/RegionService'
import to from 'await-to-js'

export default function RegionSelect ({onChange, initialValue}) {
  const [regions, setRegions] = React.useState([])
  const [id, setId] = React.useState('')

  const name = 'regionId'

  async function fetchData() {
    const [err, data] = await to(getRegions())
    if (err) {
      console.error(err)
      return
    }
    setRegions(data)
    const selectedId = initialValue || data[0]?.id
    if(selectedId){
      setId(selectedId)
      onChange({
        target: {
          value: selectedId,
          name
        }
      })
    }
  }

  useEffect(() => {
    fetchData();
  }, [])

  useEffect(() => {
    fetchData();
  }, [initialValue])

  const handleChange = (event) => {
    setId(event.target.value)
    onChange(event)
  }

  return (
    <>
      <FormControl sx={{
        my: 1,
        width: '40ch'
      }}>
        <InputLabel id="region-select-label">Select region</InputLabel>
        <Select
          labelId="region-select-label"
          id="region-select"
          value={id}
          name={name}
          label="Select region"
          onChange={handleChange}
        >
          {regions?.map((region) => (
            <MenuItem key={region.id} value={region.id}>{region.regionName}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  )
}
