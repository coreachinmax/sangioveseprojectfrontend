import * as React from 'react'
import { useEffect } from 'react'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import to from 'await-to-js'
import { getWineTypes } from '../../services/WineTypeService'

export default function TypeSelect ({onChange, initialValue}) {
  const [id, setId] = React.useState('')
  const [types, setTypes] = React.useState([])

  const name = 'typeId'

  async function fetchData () {
    const [err, data] = await to(getWineTypes())
    if (err) {
      console.error(err)
      return
    }

    setTypes(data)
    const selectedId = initialValue || data[0]?.id
    if(selectedId){
      setId(selectedId)
      onChange({
        target: {
          value: selectedId,
          name
        }
      })
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  useEffect(() => {
    fetchData();
  }, [initialValue])

  const handleChange = (event) => {
    setId(event.target.value)
    onChange(event)
  }

  return (
    <>
      <FormControl sx={{
        my: 1,
        width: '40ch'
      }}>
        <InputLabel id="type-select-label">Select wine type</InputLabel>
        <Select
          labelId="type-select-label"
          id="type-select"
          value={id}
          name={name}
          label="Select wine type"
          onChange={handleChange}
        >
          {types?.map((type) => (
            <MenuItem key={type.id} value={type.id}>{type.wineTypeName}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  )
}
