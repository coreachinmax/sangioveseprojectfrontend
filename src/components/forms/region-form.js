import * as React from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import Container from '@mui/material/Container'
import {useNavigate, useParams} from "react-router-dom";
import Button from "@mui/material/Button";
import {createWine} from "../../services/WineService";
import {createRegion, getRegion, updateRegion} from "../../services/RegionService";
import {useEffect} from "react";
import to from "await-to-js";
import {useSnackbar} from "notistack";

export default function RegionForm() {
    const {enqueueSnackbar} = useSnackbar()
    const routerParams = useParams()
    const [data, setData] = React.useState({
        regionName: ''
    })

    useEffect(() => {
        if (routerParams.id) {
            fetchData(routerParams.id)
        }
    }, [])

    async function fetchData (id) {
        const [err, data] = await to(getRegion(id))

        if (err) {
            console.error(err)
            return
        }

        setData(data)
    }

    const navigate = useNavigate()

    const pages = [{name: 'Wines', to: "/create-wine"}, {name: 'Wine types', to: "/create-wine-type"}];

    async function upsertRegion() {
        let err;
        if (routerParams.id) {
            [err] = await to(updateRegion(routerParams.id, data))
        } else {
            [err] = await to(createRegion(data))
        }

        if(err){
            enqueueSnackbar(JSON.stringify(err.response?.data?.errors), {variant: 'error'})
            return
        }

        enqueueSnackbar('Region has been successfully upserted.', {variant: 'success'})
        navigate('/region-table')
    }

    function handleDataChange(event) {
        setData({...data, ...{[event.target.name]: event.target.value}})
    }

    return (
        <Container maxWidth="xl">
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': {my: 1, width: '40ch'},
                }}
                noValidate
                autoComplete="off"
            >
                {pages.map((page) => (
                    <Button variant="outlined" color="secondary" sx={{
                        mt: 2, mr: 2
                    }} key={page.name} onClick={() => navigate(page.to)}>
                        {page.name}
                    </Button>
                ))}
                <h2>{routerParams.id ? "Update Region" : "Add Region"}</h2>
                <div>
                    <TextField id="outlined-basic" label="Region name" name="regionName" variant="outlined"
                               value={data.regionName}
                               onChange={handleDataChange}/>
                </div>

                <Button variant="outlined" color="secondary" sx={{
                    mt: 2
                }} onClick={upsertRegion}>Submit</Button>
            </Box>
        </Container>
    )
}
