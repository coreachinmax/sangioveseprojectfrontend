import * as React from 'react'
import { useEffect } from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import { createWine, getWine, updateWine } from '../../services/WineService'
import Container from '@mui/material/Container'
import TypeSelect from './type-select'
import RegionSelect from './region-select'
import { useNavigate, useParams } from 'react-router-dom'
import to from 'await-to-js'
import { useSnackbar } from 'notistack'

export default function WineForm () {
  const {enqueueSnackbar} = useSnackbar()
  const routerParams = useParams()
  const [data, setData] = React.useState({
    itemName: '',
    description: '',
    imageLink: '',
    price: 0,
    regionId: null,
    typeId: null
  })
  // Used when updating an existing item
  const [regionId, setRegionId] = React.useState(null)
  const [typeId, setWineTypeId] = React.useState(null)

  useEffect(() => {
    if (routerParams.id) {
      fetchData(routerParams.id)
    } else {
      setData({
        itemName: '',
        description: '',
        imageLink: '',
        price: 0,
        regionId: null,
        typeId: null
      })
      setWineTypeId(null)
      setRegionId(null)
    }
  }, [routerParams.id])

  async function fetchData (id) {
    const [err, data] = await to(getWine(id))

    if (err) {
      console.error(err)
      return
    }

    setData({
      itemName: data.itemName,
      description: data.description,
      imageLink: data.imageLink,
      price: data.price,
      regionId: data.region.id,
      typeId: data.wineType.id
    })
    setWineTypeId(data.wineType.id)
    setRegionId(data.region.id)
  }

  const navigate = useNavigate()

  const pages = [{name: 'Regions', to: '/create-region'}, {
    name: 'Wine types',
    to: '/create-wine-type'
  }, {name: 'Wines table', to: '/wine-table'}]

  async function upsertWine () {
    // console.log(data)
    let err;
    if (routerParams.id) {
      [err] = await to(updateWine(routerParams.id, data))
    } else {
      [err] = await to(createWine(data))
    }

    if(err){
      enqueueSnackbar(JSON.stringify(err.response?.data?.errors), {variant: 'error'})
      return
    }

    // variant could be success, error, warning, info, or default
    enqueueSnackbar('Wine has been successfully upserted.', {variant: 'success'})
    navigate('/wine-table')
  }

  function handleDataChange (event) {
    setData({...data, ...{[event.target.name]: event.target.value}})
  }

  function handlePriceChange (event) {
    const price = event.target.value < 0 ? 0 : event.target.value
    setData({...data, ...{price}})
  }

  return (
    <Container maxWidth="xl">
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': {my: 1, width: '40ch'},
        }}
        noValidate
        autoComplete="off"
      >
        {pages.map((page) => (
          <Button variant="outlined" color="secondary" sx={{
            mt: 2, mr: 2
          }} key={page.name} onClick={() => navigate(page.to)}>
            {page.name}
          </Button>
        ))}
        <h2>{routerParams.id ? 'Update Wine' : 'Add Wine'}</h2>
        <div>
          <TextField id="outlined-basic" label="Wine name" name="itemName" variant="outlined"
                     value={data.itemName}
                     onChange={handleDataChange}/>
        </div>
        <div>
          <TextField
            id="outlined-multiline-flexible"
            label="Description"
            name="description"
            value={data.description}
            multiline
            maxRows={4}
            onChange={handleDataChange}
          />
        </div>
        <div>
          <TextField id="outlined-basic" label="Image link" variant="outlined" name="imageLink"
                     value={data.imageLink} onChange={handleDataChange}/>
        </div>
        <div>
          <TextField
            id="outlined-number"
            label="Price (in cents)"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            value={data.price}
            onChange={handlePriceChange}
          />
        </div>

        <div>
          {(!routerParams.id || routerParams.id && typeId) &&
            <TypeSelect onChange={handleDataChange} initialValue={typeId}></TypeSelect>}
        </div>

        <div>
          {(!routerParams.id || routerParams.id && typeId) &&
            <RegionSelect onChange={handleDataChange} initialValue={regionId}></RegionSelect>}
        </div>

        <Button variant="outlined" color="secondary" sx={{
          mt: 2
        }} onClick={upsertWine}>Submit</Button>
      </Box>
    </Container>
  )
}
