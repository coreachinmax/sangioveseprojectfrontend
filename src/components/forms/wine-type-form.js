import * as React from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import Container from '@mui/material/Container'
import Button from "@mui/material/Button";
import {useNavigate, useParams} from "react-router-dom";
import {createWineType, getWineType, updateWineType} from "../../services/WineTypeService";
import {useEffect} from "react";
import to from "await-to-js";
import { useSnackbar } from 'notistack'

export default function WineTypeForm() {
    const {enqueueSnackbar} = useSnackbar()
    const routerParams = useParams()
    const [data, setData] = React.useState({
        wineTypeName: ''
    })

    useEffect(() => {
        if (routerParams.id) {
            fetchData(routerParams.id)
        }
    }, [])

    async function fetchData (id) {
        const [err, data] = await to(getWineType(id))

        if (err) {
            console.error(err)
            return
        }

        setData(data)
    }

    const pages = [{name: 'Regions', to: "/create-region"}, {name: 'Wines', to: "/create-wine"}];

    const navigate = useNavigate()

    async function upsertWineType() {
        let err;
        if (routerParams.id) {
            [err] = await to(updateWineType(routerParams.id, data))
        } else {
            [err] = await to(createWineType(data))
        }

        if(err){
            enqueueSnackbar(JSON.stringify(err.response?.data?.errors), {variant: 'error'})
            return
        }

        enqueueSnackbar('Wine type has been successfully upserted.', {variant: 'success'})
        navigate('/wine-type-table')
    }

    function handleDataChange(event) {
        setData({...data, ...{[event.target.name]: event.target.value}})
    }

    return (
        <Container maxWidth="xl">
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': {my: 1, width: '40ch'},
                }}
                noValidate
                autoComplete="off"
            >
                {pages.map((page) => (
                    <Button variant="outlined" color="secondary" sx={{
                        mt: 2, mr: 2
                    }} key={page.name} onClick={() => navigate(page.to)}>
                        {page.name}
                    </Button>
                ))}
                <h2>{routerParams.id ? "Update Wine Type" : "Add Wine Type"}</h2>
                <div>
                    <TextField id="outlined-basic" label="Wine type name" name="wineTypeName" variant="outlined"
                               value={data.wineTypeName}
                               onChange={handleDataChange}/>
                </div>

                <Button variant="outlined" color="secondary" sx={{
                    mt: 2
                }} onClick={upsertWineType}>Submit</Button>
            </Box>
        </Container>
    )
}
