import * as React from 'react'
import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TablePagination from '@mui/material/TablePagination'
import TableRow from '@mui/material/TableRow'
import Container from '@mui/material/Container'
import to from 'await-to-js'
import { deleteWine, getWines } from '../../services/WineService'
import { useEffect } from 'react'
import { centsToDollars } from '../../helpers/price'
import IconButton from '@mui/material/IconButton'
import EditIcon from '@mui/icons-material/Edit'
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'
import { useNavigate } from 'react-router-dom'
import Button from '@mui/material/Button'

const columns = [
  {
    id: 'imageLink',
    label: 'Image',
    minWidth: 130,
    align: 'center',
  },
  {id: 'itemName', label: 'Wine name', minWidth: 170},
  {
    id: 'price',
    label: 'Price',
    minWidth: 170,
    align: 'left',
  },
  {
    id: 'region',
    label: 'Region',
    minWidth: 100,
    align: 'left',
  },
  {
    id: 'wineType',
    label: 'Wine type',
    minWidth: 100,
    align: 'left',
  },
  {
    id: 'actions',
    label: 'Actions',
    minWidth: 60,
    align: 'center',
  },
]

export default function WineTable () {
  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(10)
  const [rows, setRows] = React.useState([])

  const navigate = useNavigate()

  const pages = [{name: 'Region table', to: '/region-table'}, {
    name: 'Wine type table',
    to: '/wine-type-table'
  }, {name: 'Wines form', to: '/create-wine'}]

  async function fetchData () {
    const [err, data] = await to(getWines())

    if (err) {
      console.error(err)
      return
    }
    setRows(data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  async function deleteItem(id) {
    const [err] = await to(deleteWine(id))

    if (err) {
      console.error(err)
      return
    }

    fetchData ()
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  return (
    <Container maxWidth="xl">
      {pages.map((page) => (
        <Button variant="outlined" color="secondary" sx={{
          mt: 2, mr: 2
        }} key={page.name} onClick={() => navigate(page.to)}>
          {page.name}
        </Button>
      ))}
      <Paper sx={{my: 1, overflow: 'hidden'}}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{minWidth: column.minWidth}}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      <TableCell align="center"><img src={row.imageLink}
                                                     height="100"/></TableCell>
                      <TableCell>{row.itemName}</TableCell>
                      <TableCell>{centsToDollars(row.price)}$</TableCell>
                      <TableCell>{row.region.regionName}</TableCell>
                      <TableCell>{row.wineType.wineTypeName}</TableCell>
                      <TableCell>
                        <IconButton aria-label="edit" onClick={() => navigate(`/update-wine/${row.id}`)}>
                          <EditIcon/>
                        </IconButton>
                        <IconButton aria-label="delete" color="error" onClick={() => deleteItem(row.id)}>
                          <DeleteOutlineIcon/>
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  )
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Container>
  )
}
